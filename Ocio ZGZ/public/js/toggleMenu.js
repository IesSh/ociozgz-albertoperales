function toggleMenu() {
    // Al hacer click, se sustituirán los iconos y se hará un slide
    $("#hamburger").click(function () {
        if ($('#hamburger').attr('class') == 'fa fa-bars') {
            $('#hamburger').removeClass('fa fa-bars').addClass('fa fa-close')
        } else {
            $('#hamburger').removeClass('fa fa-close').addClass('fa fa-bars')
        }
        $('.nav').slideToggle();
    });

}

//Intento de capturar las medidas y de mostrar el menú si supera la medida de móvil
function getScreenSize() {
    screenWeight = $(document).width();
    screenHeight = $(document).height();
    if (screenHeight > 540) {
        $('.nav').slideDown();
    }
}
