<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct($data)
    {
        $this->name = $data['name'];
        $this->subject = $data['subject'];
        $this->email = $data['email'];
        $this->msg = $data['msg'];
    }

    public function build()
    {
        return $this->view('client.mail.ContactUsNotification')
            ->from($this->email, $this->name)
            ->subject($this->subject)
            ->with([
                'name' => $this->name,
                'subject' => $this->subject,
                'email' => $this->email,
                'msg' => $this->msg
            ]);
    }
}
