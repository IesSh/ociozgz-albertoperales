<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Event;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserEventBookingPolicy
{
    use HandlesAuthorization;

    public function owner(User $user, Event $event) {
        return $user->id == $event->user_id;
    }
}
