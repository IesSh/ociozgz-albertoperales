<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use App\Models\Booking;

class EventController extends Controller
{

    // Function to check if event has expired
    public function checkDate()
    {
        $date = \Carbon\Carbon::createFromTimestamp(strtotime($this->date->format('Y-m-d') . $this->max_hour->format('H:i:s')));
        $now = \Carbon\Carbon::now();

        if ($date > $now) {
            $this->state = false;
            $this->save();
            return $this->state;
        }
    }

    // View that shows all the events I've created: ADMIN PANEL 
    public function index()
    {
        $user = \Auth::user();
        $events = Event::orderBy('id', 'desc');
        return view('admin.event.index', ['events' => $events, 'user' => $user]);
    }

    // Create events: ADMIN PANEL
    public function create()
    {
        $user = \Auth::user();
        return view('admin.event.create', ['user' => $user]);
    }

    // Function to store events: ADMIN PANEL
    public function store(Request $request)
    {
        // Form validation
        $rules = [
            'name' => 'required|string|max:30',
            'date' => 'required',
            'max_hour' => 'required',
            'location' => 'required|string',
            'capacity' => 'required|integer',
            'price' => 'required',
            'image_path' => 'mimes:jpg,jpeg,png,gif'
        ];

        // New event
        $event = new Event;
        $event->user_id = $request->user()->id;
        $event->date = $request->date;
        $event->max_hour = $request->max_hour;
        $event->name = $request->name;
        $event->description = $request->description;
        $event->location = $request->location;
        $event->capacity = $request->capacity;
        $event->state = 1;
        $event->restantes = $request->capacity;
        $event->price = $request->price;

        $image_path = $request->file('image_path');

        // Upload flyer to storage
        if ($image_path) {
            $image_path_full = time() . $image_path->getClientOriginalName();
            Storage::disk('flyers')->put($image_path_full, File::get($image_path));
            $event->image_path = $image_path_full;
        }

        $request->validate($rules);
        $event->save();
        return redirect('/admin/eventos')->with([
            'message' => 'Evento creado correctamente'
        ]);
    }

    // View for getting bookings of an specific event: ADMIN PANEL
    public function evento_reservas($event_id)
    {
        $user = \Auth::user();
        $event = Event::find($event_id);
        $bookings = Booking::where('event_id', $event_id)->get();

        $this->authorize('owner', $event, $bookings);
        return view('admin.eventBooking.index', compact('bookings', 'event_id', 'event', 'user'));
    }

    // Show an event: ADMIN PANEL
    public function show($id)
    {
        $user = \Auth::user();
        $event = Event::find($id);
        $url = "http://http://127.0.0.1:8000/". $_SERVER['REQUEST_URI'];
        // Check url so that the user will be redirected to the correct one
        if (strpos($url, 'admin')) {
            $this->authorize('owner', $event);
            return view('admin.event.show', ['event' => $event, 'user' => $user]);
        } else {
            return view ('client.event.show', ['event' => $event]);
        }

    }

    // Edit an event: ADMIN PANEL
    public function edit($id)
    {
        $user = \Auth::user();
        $event = Event::find($id);
        $this->authorize('owner', $event);
        return view('admin.event.edit', ['event' => $event, 'user' => $user]);
    }

    // Function update events: ADMIN PANEL
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|string|max:30',
            'date' => 'required',
            'max_hour' => 'required',
            'location' => 'required|string',
            'capacity' => 'required|integer',
            'price' => 'required',
            'image_path' => 'mimes:jpg,jpeg,png,gif'
        ];

        $event = Event::findOrFail($id);
        $event->user_id = $request->user()->id;
        $event->date = $request->date;
        $event->name = $request->name;
        $event->max_hour = $request->max_hour;
        $event->description = $request->description;
        $event->location = $request->location;
        $event->capacity = $request->capacity;
        $event->restantes = $event->restantes;
        $event->state = $event->state;
        $event->price = $request->price;

        //Almacenar Flyer
        $image_path = $request->file('image_path');
        if ($image_path) {
            //Nombre unico
            $image_path_full = time() . $image_path->getClientOriginalName();
            //Almacenar en la carpeta 
            Storage::disk('flyers')->put($image_path_full, File::get($image_path));
            $event->image_path  = $image_path_full;
        }


        $request->validate($rules);
        $event->update();

        return redirect('/admin/eventos')->with('message', 'Evento actualizado correctamente');
    }

    // Get flyer image
    public function getImage($filename)
    {
        $file = Storage::disk('flyers')->get($filename);
        return new Response($file, 200);
    }

    // Function to destroy an event: ADMIN PANEL
    public function destroy($id)
    {
        $event = Event::find($id);
        Storage::disk('flyers')->delete($event->image_path);
        Event::destroy($id);
        return back()->with('message', 'Evento borrado');
    } 
}
