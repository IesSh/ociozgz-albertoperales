<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\User;
class UserEventBookingController extends Controller
{

    public function index()
    {
        $user = \Auth::user();
        $events = Event::where('user_id', $user->id)->get();
        return view('admin.userEventBooking.index', compact('events'));
    }

    // Mostrar eventos en el cliente
    public function showEvents($user_id)
    {
        $users = User::all();
        $user = User::find($user_id);
        $events = Event::where('user_id', $user->id)->get();
        return view('client.event.index', compact('events', 'user_id', 'user', 'users'));
    }
}
