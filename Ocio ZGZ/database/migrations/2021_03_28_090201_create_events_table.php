<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{

    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); //FK User ID
            $table->date('date');
            $table->time('max_hour');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('location');
            $table->string('image_path')->nullable();
            $table->integer('capacity');
            $table->integer('restantes');
            $table->boolean('state')->default(true); // true = Abierto
            $table->float('price');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('events');
    }
}
