<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Booking;

class BookingSeeder extends Seeder
{

    public function run()
    {
        Booking::create([
            'user_id' => '1',
            'event_id' => '1',
            'asistentes' => 4,
            'price' => 50,
        ]);

        Booking::create([
            'user_id' => '1',
            'event_id' => '1',
            'asistentes' => 6,
            'price' => 30,
        ]);

    }
}
