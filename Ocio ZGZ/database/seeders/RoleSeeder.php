<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{

    public function run()
    {
        Role::create([
            'id' => '1',
            'name' => 'usuario'
        ]);

        Role::create([
            'id' => '2',
            'name' => 'encargado'
        ]);
        
        Role::create([
            'id' => '3',
            'name' => 'administrador'
        ]);
    }
}
