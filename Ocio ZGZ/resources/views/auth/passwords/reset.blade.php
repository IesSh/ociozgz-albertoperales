<html lang="es">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
@extends('auth.login')
@section('title', 'Recuperar contraseña')

<style>
    /*VERIFICACIÓN*/
    * {
        margin: 0;
        padding: 0;
        font-family: 'Montserrat';
    }

    .verify-container {
        position: absolute;
        width: 100vw;
        height: 100vh;
        background-color: rgba(0, 0, 0, 0.7);
        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 99999;
    }

    .verify-dialog {
        width: 42%;
        height: 80%;
        border-radius: 10px;
        background-color: rgb(242, 242, 242);
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.3);
        overflow: hidden;
        background-color: #fff;
        position: relative;
        text-align: left;
        overflow-y: auto;
    }

    .verify-dialog-header {
        height: 7em;
        width: 100%;
        background-image: linear-gradient(90deg, #ffbb00, #ff137d), url("https://images.pexels.com/photos/989711/pexels-photo-989711.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940");
        background-size: cover;
        background-blend-mode: lighten;
    }

    .verify-dialog-body {   
        padding: 2em 3em 1em 3em;
    }

    .verify-btn-container {
        margin-top: 1.5em;
    }

    #verify-logo {
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    button {
        padding: 0.7em;
        border: none;
        cursor: pointer;
        background: linear-gradient(to left, #ffc62a, #ff62c6);
        color: #fff;
        width: 100%;
        border-radius: 20px;
    }

    .enviar-enlace {
        justify-content: center;
        margin-top: 1em;
        width: 100%;
    }
    
    @media screen and (max-width: 768px) {
            .verify-dialog {
                width: 80%;
            }

        }
</style>

<div class="verify-container">
    <div class="verify-dialog">
        <div class="verify-dialog-header"></div>
        <i id="close" class="fa fa-times-circle" style="position: absolute; top: 10; right:10; font-size:2.3em;color:white; cursor:pointer"></i>
        <div class="verify-dialog-body">
            <form method="POST" style="display:flex; flex-direction:column; justify-content:center; align-items:center" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">

                <div style=" width:100%">
                    <label for="email">{{ __('E-Mail') }}</label><br>
                    <input id="email" style="margin-top:10px; margin-bottom:10px; width:100%" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                    <span class="error-message" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div style=" width:100%">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Nueva Contraseña') }}</label><br>
                    <input id="password" style="margin-top:10px; margin-bottom:10px; width:100%" placeholder="Contraseña" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                    @error('password')
                    <span class="error-message" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div style=" width:100%">
                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar contraseña') }}</label><br>
                        <input id="password-confirm" style="margin-top:10px; margin-bottom:10px; width:100%"  placeholder="Confirmar contraseña" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                </div>

                <div style="margin-top:2em; width:60%; margin:1em auto;">
                    <button type="submit">
                        {{ __('Resetear Contraseña') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>