@extends('auth.login')
@section('title', 'Verificación')

<style>
    /*VERIFICACIÓN*/
    .verify-container {
        position: absolute;
        width: 100vw;
        height: 100vh;
        background-color: rgba(0, 0, 0, 0.7);
        backdrop-filter: blur(5px);
        display: flex;
        justify-content: center;
        align-items: center;
        z-index: 99999;
    }

    .verify-dialog {
        width: 42%;
        height: 80%;
        border-radius: 10px;
        background-color: rgb(242, 242, 242);
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.3);
        overflow: hidden;
        background-color: #fff;
        position: relative;
        text-align: center;
        overflow-y: auto;
    }

    .verify-dialog-header {
        height: 7em;
        width: 100%;
        background-image: linear-gradient(90deg, #ffbb00, #ff137d), url("https://images.pexels.com/photos/989711/pexels-photo-989711.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940");
        background-size: cover;
        background-blend-mode: lighten;
    }

    .verify-dialog-body {
        padding: 3em;
        padding-top: 2em;
    }

    .verify-btn-container {
        margin-top: 1.5em;
    }
    
    #verify-logo {
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    button {
        padding: 0.7em;
        border: none;
        cursor: pointer;
        background: linear-gradient(to left, #ffc62a, #ff62c6);
        color: #fff;
        width: 90%;
        border-radius: 20px;
        transition: ease-in-out 0.1s;
    }
    button:hover {
        box-shadow: 0px 0px 20px -2px #ff62c6;
        transform: scale(1.05);
    }
    
    .enviar-enlace {
        justify-content: center;
        margin-top: 1em;
        width: 100%;
    }
</style>

<div class="verify-container">
    <div class="verify-dialog">
        <div class="verify-dialog-header"></div>
        <div class="verify-dialog-body">
            <div id="verify-logo">
                <img src="{{ asset('img/logo.svg') }}" alt="logo ocio zgz" width="200px" height="120px" ">
            </div>
            <h2>¡Listo! Revisa tu correo electrónico</h2><br>
            <p>Para continuar a Ocio ZGZ se requiere una verificación de correo.  Por favor revisa tu buzón de correo y sigue las instrucciones enviadas &#128233;</p>
            
            <form class="verify-btn-container" method="POST" action="{{ route('verification.resend') }}">
                @csrf
                <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('Enviar correo') }}</button>
            </form>
            @if (session('resent'))
                <div class="enviar-enlace" role="alert">
                    <p style="font-size: 0.9em;"><strong><i class="fa fa-info-circle" style="margin-right: 5px;"></i>Se ha enviado un enlace de verificación a tu correo</p></strong>
                </div>
            @endif
            </div>
        </div>
    </div>
</div>