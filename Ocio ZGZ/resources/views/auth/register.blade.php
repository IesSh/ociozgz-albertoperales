<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrarse - Ocio ZGZ</title>
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    <link rel="stylesheet" stype="text/css" href="./css/normalize.css">
    <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.css') }}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type=" text/javascript" src="{{ asset('js/loader.js') }}"></script>

    <style>
        @import url("https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,700;1,400&display=swap");

        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
            font-family: 'Montserrat';
        }

        /*Definición de estilos de los loader*/
    .pre-loader-container {
        background-color: #fff;
        position: absolute;
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 99999;
    }

    .pre-loader-container .pre-loader {
        position: absolute;
        top: 52%;
        left: 50%;
    }

    .pre-loader-container .pre-loader span {
        height: 1.3em;
        width: 1.3em;
        display: block;
        position: absolute;
        left: 0;
        top: 0;
        filter: brightness(1.2);
        border-radius: 50%;
        animation: wave 2s ease-in-out infinite;
    }

    .pre-loader-container .pre-loader span:nth-child(1) {
        left: -4em;
        animation-delay: 0s;
        background: linear-gradient(to right, #ff9600, #ff8e15);
    }

    .pre-loader-container .pre-loader span:nth-child(2) {
        left: -2em;
        animation-delay: 0.1s;
        background: linear-gradient(to right, #ff7d14, #ff6d2f);
    }

    .pre-loader-container .pre-loader span:nth-child(3) {
        left: 0em;
        animation-delay: 0.2s;
        background: linear-gradient(to right, #ff5538, #ff414d);
    }

    .pre-loader-container .pre-loader span:nth-child(4) {
        left: 2em;
        animation-delay: 0.3s;
        background: linear-gradient(to right, #ff3059, #ff1c6e);
    }

    @keyframes wave {

        0%,
        75%,
        100% {
            transform: translateY(0) scale(1);
        }

        25% {
            transform: translateY(1em);
        }

        50% {
            transform: translateY(-1.1em) scale(1.1);
        }
    }

        .container {
            display: flex;
            flex-direction: row;
        }

        #banner {
            width: 50vw;
            height: 100vh;
            background-image: linear-gradient(90deg, #ffbb00, #ff137d),url("{{asset('img/about-us.jpeg')}}");
            background-size: cover;
            background-blend-mode: lighten;
        }

        .form-container {
            width: 50vw;
            height: 100vh;
            /* border: 5px solid red; */
            position: relative;
            overflow-y: scroll;

        }

        .form {
            width: 60%;
            height: 70%;
            /* border: 1px solid red; */
            margin: auto;
            margin-top: 10%;
            padding-bottom: 5em;

        }

        .form-field {
            display: flex;
            flex-direction: column;
            margin-bottom: 1em;
            font-size: 1em;
        }

        .form-field-row {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
        }

        .form-btn {
            margin: auto;
            width: 100%;
            background: linear-gradient(to left, #ffc62a, #ff62c6);
            color: #fff;
            text-align: center;
            border: none;
            height: 2.5em;
            border-radius: 50px;
        }

        body {
            font-family: "Montserrat", sans-serif;
        }

        label {
            margin-bottom: 20px;
        }

        input {
            border: none;
            background-color: rgb(236, 236, 236);
            color: rgb(126, 126, 126);
            padding: 10px;
            height: 2.5em;
        }

        form {
            margin-top: 2em;
        }

        input:focus,
        input[type]:focus {
            outline: 0 none;
        }

        h1 {
            background: -webkit-linear-gradient(left, rgba(255, 93, 177, 1) 0%, rgba(248, 132, 0, 1) 100%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            text-align: center;
        }

        #passForgot {
            margin-top: 2em;
            padding-top: 10px;
            text-align: center;
            border-top: 2px solid rgb(211, 211, 211);

        }

        .btn-container {
            margin-top: 10px;
            padding-top: 10px;
            text-align: center;
        }

        a {
            text-decoration: none;
            font-size: 0.9em;
            color: rgb(167, 167, 167);
        }

        .error-message {
            color: tomato;
            margin-top: 10px
        }

        @media screen and (max-width: 768px) {
            .container {
                flex-direction: column;
            }
            #banner {
                width: 100%;
                height: 20vh;
            }
            .form-container {
                margin: 0 auto;
            }
            .form {
                width: 100%;
            }
            #logo img{
                width: 90%;
                margin-top: 1em;
            }
        }
    </style>

</head>

<body>
    <div class="container">
    <div class="pre-loader-container" id="loader">
        <div class="pre-loader">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
        <!-- <p style="font-size: 1.3em;">Cargando  ...</p> -->
    </div>
        <div id="banner"></div>
        <div class="form-container">
            <div class="form">
                <div id="logo" style="text-align: center; margin-bottom: 10px">
                    <a href="/">
                        <img src="{{ asset('img/logo.svg') }}" alt="logo ocio zgz" width="200px" height="100px">
                    </a>
                </div>
                <h1>Registrarse en Ocio ZGZ</h1>
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                        <div class="form-field">
                            <label for="name">Nombre</label>
                            <input type="text" name="name" placeholder="Introduce tu nombre" value="{{ old('name') }}">

                            @error('name')

                            <span class="invalid-feedback" role="alert">
                                <p class="error-message"><strong>{{ $message }}</strong></p>
                            </span>
                            @enderror
                        </div>
                        <div class="form-field">
                            <label for="surname">Apellidos</label>
                            <input type="text" name="surname" placeholder="Introduce tus apellidos" value="{{ old('surname') }}">

                            @error('surname')

                            <span class="invalid-feedback" role="alert">
                                <p class="error-message"><strong>{{ $message }}</strong></p>
                            </span>
                            @enderror
                        </div>

                        <div class="form-field">
                            <label for="nick">Nick</label>
                            <input type="text" placeholder="Introduce tu nick" name="nick" value="{{ old('nick') }}">
                            @error('nick')

                            <span class="invalid-feedback" role="alert">
                                <p class="error-message"><strong>{{ $message }}</strong></p>
                            </span>
                            @enderror
                        </div>

                        <div class="form-field">
                            <label for="email">Email</label>
                            <input type="text" name="email" placeholder="Introduce tu email" value="{{ old('email') }}">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <p class="error-message"><strong>{{ $message }}</strong></p>
                            </span>
                            @enderror
                        </div>

                    <div class="form-field">
                        <label for="pasword">Contraseña</label>
                        <input id="password" type="password" placeholder="Introduce una contraseña" name="password" required autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <p class="error-message"><strong>{{ $message }}</strong></p>
                        </span>
                        @enderror
                    </div>
                    <div class="form-field">
                        <label for="pasword_confirmation">Confirmar contraseña</label>

                        <input type="password" placeholder="Confirma contraseña" name="password_confirmation">
                        @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="btn-container">
                        <input class="form-btn" style=" cursor: pointer;" type="submit" value="Registrarse"><br>
                    </div>
                    <div id="passForgot">
                        <a href="{{ route('login') }}" style="font-weight: bold;">Login</a>
                        <!-- <br><a href="#">Recordar contraseña</a> -->
                    </div>
                </form>
            </div>
            </di>
        </div>
    </div>

</body>

</html>