@extends('admin.app')
@section('title', 'Lista de Eventos')
<!--Título dinámico-->
<style>
    .card-container {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        margin: 1em;
        height: auto;
        font-size: 0.8em;
        transition: ease-in-out 0.2s;
    }

    .card-container:hover {
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.4);
        border-collapse: collapse;
        transform: scale(1.03);
        border-radius: 20px;
    }

    .info-container {
        background-color: #fff;
        padding: 1em;
        width: 200px;
        border-top-right-radius: 20px;
        border-bottom-right-radius: 20px;
    }

    #events {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        height: auto;
        margin-bottom: 5em;
    }

    .barra {
        background: linear-gradient(to left, #ffc62a, #ff62c6);
        height: 6px;
        width: auto;
        margin-top: 10px;
        border-radius: 20px;
    }

    @media screen and (min-width: 0) and (max-width: 768px) {
        #events {
            width: 100% !important;
        }
        .card-container {
            width: 100% !important;
        }
    }

</style>
<script>
    // Alert message
</script>
@section('content')
@include('admin.includes.alert-message')


<h1 style="width: fit-content">Listado de eventos<div class="barra"></div>
</h1>
<div id="events">

    @if (!empty($user->events))
    @foreach ($user->events as $event)
    <div class="card-container">
        @if ($event->image_path)
        <img style="max-width:200px;" src="{{url('event/flyer/'.$event->image_path)}}">
        @endif
        <div class="info-container" style="position: relative; <?= (!$event->image_path) ? 'border-radius:20px;' : '' ?>">
            <div class="text-container">
                <h2 style="font-weight: bold; font-size: 2em">{{$event->name}}</h2>
                <h3 style="color: orange">{{$event->date->format('d-m-Y')}}</h3>
                <h4 style="margin-top: 1.5em;">{{$event->description}}</h4>
            </div>
            <div class="btn-container" style="margin-top: 3em;display: flex; flex-direction: column; justify-content: flex-end; width: 100%">
                <a href="/admin/eventos/{{$event->id}}" style="width: 100%; background: linear-gradient(to left,#ffc62a, #ff62c6); color: #fff; border-top-left-radius: 10px;border-top-right-radius: 10px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px; padding: 0.5em; text-align: center;margin-bottom: 0.5em;">Ver</a>
                <a href="/admin/eventos/{{$event->id}}/edit" style="width: 100%; background: linear-gradient(to left,#ffc62a, #ff62c6); color: #fff; border-top-left-radius: 10px;border-top-right-radius: 10px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px; padding: 0.5em; text-align: center; margin-bottom: 0.5em;">Editar</a>
                <a href='/admin/evento/{{$event->id}}/reservas' style=" width: 100%; background: linear-gradient(to left,#ffc62a, #ff62c6); color: #fff; border-top-left-radius: 10px;border-top-right-radius: 10px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px; padding: 0.5em; text-align: center; margin-bottom: 0.5em;">Ver reservas</a>

                <form action="/admin/eventos/{{$event->id}}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="delete">
                    <input type="submit" value="Borrar" style="width: 100%; background: linear-gradient(to left,#ffc62a, #ff62c6);  color: #fff; border-top-left-radius: 10px;border-top-right-radius: 10px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px; padding: 0.5em; text-align: center; margin-bottom: 0.5em; border: none; cursor:pointer">
                </form>
            </div>
        </div>
    </div>
    @endforeach
    @else
    <p>No hay eventos, pulse <a href="/admin/eventos/create">aquí</a> para crear uno nuevo</p>
    @endif
</div>
</div>
@endsection