@extends('admin.app')
@section('title', $event->name)
<!--Título dinámico-->
<style>
    .box {
        background-color: #fff;
        padding: 2em;
        border-radius: 5px;
        transition: ease-in-out 0.3s;
    }

    .box:hover {
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.4);
        border-collapse: collapse;
    }

    .welcome {
        display: flex;
        flex-direction: column;
        width: 100%;
        position: relative;

    }

    p {
        margin-top: 10px;
    }

    .flyer {
        transition: ease-in-out 0.3s;
        max-width: 200px;
        border-radius: 10px;
    }

    .flyer:hover {
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.4);
    }
</style>

@section('content')
<h1>Evento: {{$event->name}}</h1>
<div class="welcome">
    <div style="display: flex; flex-direction: row;">
        <div style="margin-right: 1em;">
            <li>
                <div>
                    @if ($event->image_path)
                    <img class="flyer" style="max-width:200px;" src="{{url('event/flyer/'.$event->image_path)}}">
                    @endif
                </div>
            </li>
        </div>
        <div class="box" style="position:relative;">
            <div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 20px; width: 100%;  position:absolute; bottom:0; left:0;"></div>
            <div style="white-space: nowrap;">
                <!-- <i class=" fa fa-caret-left" style="font-size: 50px; position: absolute; left:-15; color:#fff"></i> -->
                @if (!($event->state))
                <h1 style="color:tomato"><i class="fa fa-lock" style="margin-right:0.5em"></i>Evento cerrado</h1>
                @endif
                <h2 style="color: orange; margin-bottom: 10px"><i class="fa fa-info-circle" style="margin-right:10px"></i>Resumen</h2>
            </div>
            <ul>
                <li>
                    <h2>{{$event->name}}</h2>

                </li>
                <li>
                    <h2>{{$event->date->format('d-m-Y')}}

                </li>
                <li>

                    <p>{{$event->location}}</p>

                </li>

                <div style="width:5em;margin-top:2em;text-align:right; position:absolute;bottom:-3em; right:1.5em">
                    <a href="javascript:history.back()" style="
                    width: 100%;
                    background: linear-gradient(to left, #ffc62a, #ff62c6);
                    color: #fff;
                    text-align: center;
                    border: none;
                    margin-top: 10px; padding:0.7em; border-radius:20px; padding-left:2em;padding-right:2em;">Atrás
                    </a>
                </div>
            </ul>
        </div>
    </div>

</div>

@endsection