@extends('admin.app')
@section('title', 'Edición de evento')

<!--Título dinámico-->
@section('content')
<style>
    .container-form {
        width: 80%;
        margin-bottom: 6em;
        border-radius: 10px;
        display: flex;
        flex-direction: row;
    }

    main {
        max-width: 100%;
    }

    .box {
        background-color: #fff;
        margin-left: 2em;
        padding: 2em;
        border-radius: 5px;
        transition: ease-in-out 0.3s;
        width: 50%;
    }

    .box:hover {
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.4);
        border-collapse: collapse;
    }

    input[type="text"],
    input[type="date"],
    input[type="time"],
    input[type="checkbox"] {
        background-color: rgb(229, 229, 229);
        height: 2.5em;
        border: none;
        padding: 10px;
    }

    label {
        margin-bottom: 10px;
    }

    .form {
        margin: 20px;
        margin-left: 0;
        display: flex;
        flex-direction: column;
    }

    .btn-form {
        width: 100%;
        background: linear-gradient(to left, #ffc62a, #ff62c6);
        color: #fff;
        text-align: center;
        border: none;
        height: 2.5em;
    }

    .error-message {
        color: tomato;
        margin-top: 10px
    }

    #welcome {
        margin-top: 20px;
    }

    .card-container {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        font-size: 0.8em;
        transition: ease-in-out 0.2s;
        width: fit-content;
        height: fit-content;
        margin: auto;
        margin-top: 25%;
    }

    .card-container:hover {
        border-collapse: collapse;
        transform: scale(1.05);
        border-radius: 20px;
    }

    .info-container {
        background-color: #fff;
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.4);
        padding: 1em;
        width: 200px;
        border-top-right-radius: 20px;
        border-bottom-right-radius: 20px;
    }

    .barra {
        background: linear-gradient(to left, #ffc62a, #ff62c6);
        height: 6px;
        width: 100%;
        margin-top: 10px;
        border-radius: 20px;
    }
    
    @media screen and (max-width: 768px) {
        .container-form {
            flex-direction: column;
            width: 100%;
            margin: auto;
        }
        .box {
            width:100%;
            margin:0;
        }
        .card-container {
            width: 100% !important;
        }
        #previa {
            margin-top: 2em;
            width: 100% !important;
        }
        .card-container {
            flex-direction: column-reverse;
        }
        .info-container  {
            border-top-left-radius: 10px !important;
            border-top-right-radius: 10px !important;
            border-bottom-right-radius: 0 !important;
        }
        
    }
    
</style>
<div id="welcome">
    <div class="container-form">
        <form action="/admin/eventos/{{$event->id}}" method="post" class="box" enctype="multipart/form-data">
            <h1>Editar evento: {{$event->name}}
                <div class="barra"></div>
            </h1>
            @csrf
            <input type="hidden" name="_method" value="PUT">

            <div class="form">
                <label for="name"><strong>Nombre<span style="color: tomato;"> *</span></strong></label>
                <input placeholder="Nombre del evento" type="text" name="name" value="{{$event->name}}">
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>
            <div class="form">
                <label for="description"><strong>Descripción</strong></label>
                <input type="text" placeholder="Descripción del evento" name="description" value="{{$event->description}}">
            </div>

            <div class="form">
                <label for="date"><strong>¿Para qué día? <span style="color: tomato;"> *</span></strong></label>
                <input type="date" name="date"  value="{{$event->date->format('Y-m-d')}}">
                @error('date')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>
            <div class="form">
                <label for="max_hour"><strong>¿Hora máxima?<span style="color: tomato;"> *</span></strong></label>
                <input type="time" name="max_hour"  value="{{$event->max_hour->format('H:i')}}">
                @error('max_hour')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>

            <div class="form">
                <label for="location"><strong>¿Dónde se celebra?</strong></label>
                <input type="text" placeholder="Localización del evento" name="location"  value="{{$event->location}}">
            </div>


            <div class="form">
                <label for="capacity"><strong>Capacidad máxima</strong></label>
                <input type="text" placeholder="Capacidad máxima de reservas" name="capacity" value="{{$event->capacity}}">
            </div>
            <div class="form">
                <label for="price"><strong>Precio</strong></label>
                <input type="text" placeholder="Precio por persona" name="price" value="{{$event->price}}">
            </div>

            <div class="form">
                <label for="image_path"><strong>Flyer</strong></label>
                <input type="file" name="image_path" value="{{$event->image_path}}">
                @error('image_path')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>

            <div class="form">
                <input type="submit" class="btn-form" style="cursor: pointer;" value="Actualizar">
            </div>
        </form>

        <!--EVENTO VISUALIZACIÓN-->
        <div class="box" id="previa" style="width:fit-content;background: linear-gradient(to top,#ffc62a 0%, #ff62c6 50%,#fff 50%, #fff 100%); border-radius:20px">
            <div class="card-container">
                @if ($event->image_path)
                <img style="max-width:200px;" src="{{url('event/flyer/'.$event->image_path)}}">
                @endif
                <div class="info-container" style="position: relative;" <?= (!$event->image_path) ? 'border-radius:20px;' : '' ?>">
                    <div class="text-container">
                        <h2 style="font-weight: bold; font-size: 2em">{{$event->name}}</h2>
                        <h3 style="color: orange">{{$event->date}}</h3>
                        <h4 style="margin-top: 1.5em;">{{$event->description}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection