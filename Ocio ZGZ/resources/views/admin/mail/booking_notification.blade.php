<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<style>
    * {
        font-family: 'Montserrat';

    }

    #container {
        margin: 0 auto;
    }

    body {
        margin: 2em;
    }

    ul,
    li {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    a {
        text-decoration: none
    }

    @import url('https://fonts.googleapis.com/css2?family=Montserrat&display=swap');
</style


<body>
    <div id="container">
            <div style="text-align: center;">
                <img src="{{ asset('img/logo.svg') }}" alt="logo ocio zgz" width="200px" height="120px">
                <h2>¡Hola {{$encargado->nick}}!</h2>
            </div>
        <div style="width:50%; margin:0 auto">
            <h3><?= $user->name . ' ' . $user->surname ?> ha realizado una reserva para el evento: {{$event->name}} (celebrado el día {{$event->date->format('d-m-Y')}})</h3>
            <h3 style="color: orange;">Detalles de la reserva</h3>
            <ul>
                <li><strong>Nombre:</strong> {{$user->name}}</li>
                <li><strong>Apellidos:</strong> {{$user->surname}}</li>
                <li><strong>Asistentes:</strong> {{$booking->asistentes}}</li>
                <li><strong>Importe:</strong> {{$booking->price}}€</li>
            </ul>
        </div>
    </div>
</body>

</html>