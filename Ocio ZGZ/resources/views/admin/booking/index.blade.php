@extends('admin.app')
@section('title', 'Lista de Reservas')
<!--Título dinámico-->
<style>
    table {
        width: 100%;
        border-collapse: collapse;
    }

    th,
    td {
        width: 25%;
        text-align: left;
        vertical-align: top;
        padding: 0.3em;
        caption-side: bottom;

        border-collapse: collapse;
    }

    td {
        text-align: right;
        border-bottom: 1px solid black;
        padding: 1em;
    }

    thead {
        background: linear-gradient(to left, #ffc62a, #ff62c6);
        color: white;
        font-weight: bold;
    }

    th {
        text-align: center;
    }
</style>

@section('content')

<h1>Reservas</h1>
<div>
    @if (!empty($user->bookings))
    <table>
        <thead>

            <tr>
                <th>ID</th>
                <th>ID del Evento</th>
                <th>ID del usuario</th>
                <th>Precio</th>
                <th></th>
            </tr>
        </thead>
        @foreach ($user->bookings as $booking)
        <tr>
            <td>{{$booking->id}}</td>
            <td>{{$booking->event_id}}</td>
            <td>{{$booking->user_id}}</td>
            <td>{{$booking->price}}€</td>
            <td><a href="/admin/reservas/{{$booking->id}}" style="width: 100%; background: linear-gradient(to left,#ffc62a, #ff62c6); color: #fff; border-top-left-radius: 10px;border-top-right-radius: 10px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px; padding: 0.5em; text-align: center;margin-bottom: 0.5em;">Ver</a></td>
        </tr>
        @endforeach
    </table>
    @else
    <p>No hay reservas para este evento</p>
    @endif
</div>
</div>
@endsection