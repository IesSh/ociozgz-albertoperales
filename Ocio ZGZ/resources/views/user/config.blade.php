@extends('admin.app')
@section('title', 'Editar perfil:'.' '.$user->nick)
<style>
    .box {
        background-color: #fff;
        /* margin-left: 2em; */
        padding: 2em;
        border-radius: 5px;
        transition: ease-in-out 0.3s;
    }

    .box:hover {
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.4);
        border-collapse: collapse;

    }

    #welcome {
        display: flex;
        flex-direction: row;
    }

    input[type="text"],
    input[type="password"] {
        background-color: rgb(229, 229, 229);
        height: 2.5em;
        border: none;
        padding: 10px;
    }

    .alert-message {
        width: 100%;
        position: absolute;
        top:0;
        left: 0;
        height: 3em;
        background-color: rgb(181, 235, 204);
        border-bottom: 2px solid rgb(43, 159, 92);
        padding: 1.2em;
        display: flex;
        text-align: center;
        align-items: center;
        margin-bottom: 1em;
        color: rgb(43, 159, 92);
    }

    .error-message {
        color: tomato;
        margin-top: 10px
    }

    .form-flex {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    }

    label {
        margin-bottom: 10px;
    }

    #edit-box {
        margin-right: 2em;
    }

    form {
        width: 100%;
    }

    .form {
        margin: 20px;
        margin-left: 0;
        display: flex;
        flex-direction: column;
    }

    .btn-form {
        width: 100%;
        background: linear-gradient(to left, #ffc62a, #ff62c6);
        color: #fff;
        text-align: center;
        border: none;
        height: 2.5em;
        margin-bottom: 2em;
    }

    .current-avatar {
        width: 100px;
    }

    @media screen and (max-width: 769px) {
        #welcome {
            flex-direction: column;
        }

        #contraseña-box {
            margin-top: 2em;
        }

        #edit-box {
            margin-right: 0;
        }
    }
</style>
<!--Título dinámico-->
@include('admin.includes.alert-message')
@section('content')
<div id="welcome">
    <div class="box" id="edit-box">
        <h1 style="width: 7em;">Editar perfil<div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 6px; width: 100%; margin-top: 10px;border-radius:20px"></div>
        </h1>

        <form action="{{route('user.update')}}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="form-flex">
                <div class="form" style="width: 50%;">
                    <label for="name">Nombre</label>
                    <input type="text" name="name" value="{{Auth::user()->name}}">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <p class="error-message"><strong>{{ $message }}</strong></p>
                    </span>
                    @enderror
                </div>
                <div class="form" style="width: 50%;">
                    <label for="nick">Apellidos</label>
                    <input type="text" name="surname" value="{{Auth::user()->surname}}">
                    @error('surname')
                    <span class="invalid-feedback" role="alert">
                        <p class="error-message"><strong>{{ $message }}</strong></p>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="form">
                <label for="nick">Nick público</label>
                <input type="text" name="nick" value="{{Auth::user()->nick}}">
                @error('nick')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>
            <div class="form">
                <label for="email">Correo electrónico</label>
                <input type="text" name="email" value="{{Auth::user()->email}}">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>
<br>
            <h1 style="width: fit-content;">Perfil público<div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 6px; width: 100%; margin-top: 10px;border-radius:20px"></div>
        </h1>
            <div class="form">
                <label for="image">Imagen de perfil</label>
                @if (Auth::user()->image)
                <img src="{{url('user/avatar/'.Auth::user()->image)}}" class="current-avatar">
                @endif
                <input type="file" name="image" value="{{Auth::user()->image}}" style="margin-top: 10px;">
                @error('image')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>

            <div class="form">
                <label for="background">Imagen de cabecera</label>
                @if (Auth::user()->background)
                <img src="{{url('user/background/'.Auth::user()->background)}}" class="current-avatar">
                @endif
                <input type="file" name="background" value="{{Auth::user()->background}}" style="margin-top: 10px;">
                @error('backg ')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>
            <div class="form">
                <label for="description">Descripción</label>
                <input type="text" name="description" value="{{Auth::user()->description}}">
                @error('description')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>

            <div class="form">
                <input class="btn-form" style=" cursor: pointer;" type="submit" value="Guardar">
            </div>
        </form>
    </div>
    <div class="box" style="height:fit-content" id="contraseña-box">
        <h1 style="width: 7em;">Cambiar contraseña<div style="background: linear-gradient(to left, #ffc62a, #ff62c6); height: 6px; width: 100%; margin-top: 10px;border-radius:20px"></div>
        </h1>
        <form action="{{route('user.updatePassword')}}" method="post">
            @csrf
            <div class="form">
                <label for="password">Nueva contraseña</label>
                <input type="password" name="password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>
            <div class="form">
                <label for="password_confirmation">Confirmar ontraseña</label>
                <input type="password" name="password_confirmation">
                @error('password_confirmation')
                <span class="invalid-feedback" role="alert">
                    <p class="error-message"><strong>{{ $message }}</strong></p>
                </span>
                @enderror
            </div>
            <div class="form" style="margin-bottom: 0;">
                <input class="btn-form" style=" cursor: pointer;" type="submit" value="Guardar">
            </div>
        </form>

    </div>

</div>
@endsection