<!DOCTYPE html>
<html lang="es">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type=" text/javascript" src="{{ asset('js/loader.js') }}"></script>


<style>
    @import url('https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap');

    
    #social-nets img {
        transition: all 0.3s;
    }

    #social-nets img:hover {
        transform: scale(1.3);
        cursor: pointer;
    }

    /*Definición de estilos de los loader*/
    .pre-loader-container {
        background-color: #fff;
        position: absolute;
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        z-index: 99999;
        overflow: hidden;
    }

    .pre-loader-container .pre-loader {
        position: absolute;
        top: 52%;
        left: 50%;
    }

    .pre-loader-container .pre-loader span {
        height: 1.3em;
        width: 1.3em;
        display: block;
        position: absolute;
        left: 0;
        top: 0;
        filter: brightness(1.2);
        border-radius: 50%;
        animation: wave 2s ease-in-out infinite;
    }

    .pre-loader-container .pre-loader span:nth-child(1) {
        left: -4em;
        animation-delay: 0s;
        background: linear-gradient(to right, #ff9600, #ff8e15);
    }

    .pre-loader-container .pre-loader span:nth-child(2) {
        left: -2em;
        animation-delay: 0.1s;
        background: linear-gradient(to right, #ff7d14, #ff6d2f);
    }

    .pre-loader-container .pre-loader span:nth-child(3) {
        left: 0em;
        animation-delay: 0.2s;
        background: linear-gradient(to right, #ff5538, #ff414d);
    }

    .pre-loader-container .pre-loader span:nth-child(4) {
        left: 2em;
        animation-delay: 0.3s;
        background: linear-gradient(to right, #ff3059, #ff1c6e);
    }

    @keyframes wave {

        0%,
        75%,
        100% {
            transform: translateY(0) scale(1);
        }

        25% {
            transform: translateY(1em);
        }

        50% {
            transform: translateY(-1.1em) scale(1.1);
        }
    }

    * {
        padding: 0;
        margin: 0;
        box-sizing: border-box;

    }

    body {
        font-family: "Montserrat", sans-serif;
        background-color: #fff;
        color: #000;
    }

    p {
        font-size: 0.8em;
    }

    a {
        text-decoration: none;
        color: #000;
    }

    li {
        list-style: none;
    }

    .container {
        position: relative;
        height: 100%;
        width: 100%;
        display: grid;
        grid-template-columns: 100%;
        grid-template-rows: repeat(4, auto);
        grid-template-areas:
            "nav"
            "banner"
            "main"
            "footer";
    }

    /*NAVEGATION*/
    .container header nav {
        grid-area: nav;

    }

    .container header #banner {
        grid-area: banner;

    }

    .container main {
        grid-area: main;
        min-height: 60%;
    }

    .container footer {
        grid-area: footer;
    }


    /*HEADER*/
    header {
        height: auto;
        width: 100%;
    }

    /*Alerta Covid*/
    #covid-alert {
        overflow-x: hidden;
        width: 100%;
        background-color: #000;
        color: #fff;
        padding: 0.5em;
        height: auto;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: space-between;
        align-items: center;
    }

    #covid-alert-buttons {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        justify-content: space-between;
        width: auto;
        align-items: center;
    }

    .covid-alert-btn a {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        color: #fff;
        justify-content: space-between;
    }

    .covid-alert-btn a i {
        margin-right: 0.5em;
        color: #fff;
    }

    /*NAVIGATION*/
    #menu {
        box-shadow: 0px 0px 20px -4px rgba(0, 0, 0, 0.3);
        position: sticky;
        top: 0;
        z-index: 5;
        height: auto;
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 0em 2em 0em 2em;
        font-size: 1.1em;
    }

    #logo a img {
        padding-top: 0.5em;
        margin-left: 2em;
    }

    #logo {
        width: fit-content;
        display: flex;
        flex-direction: row;
        align-items: center;
    }

    #hamburger {
        display: none;
    }

    #menu ul {
        float: right;
        display: flex;
        justify-content: flex-end;
        flex-direction: row;
        /* justify-content: space-between; */
        width: 80%;
        align-items: center;
    }

    #menu ul li {
        width: auto;
        margin-right: 3em;

    }


    #menu ul li a:active {
        font-weight: bold;
        padding-bottom: 10px;
        border-bottom: 3px solid rgb(255, 137, 58);
    }

    #menu ul li a:hover {
        padding-bottom: 10px;
        border-bottom: 3px solid rgb(255, 137, 58);
    }

    #menu input {
        font-size: 0.9em;
        border-radius: 50px;
        outline: none;
        border: 2px solid black;
        padding: 0.1em 1em 0.1em 1em;

    }

    #banner {
        width: 100%;
        height: 72vh;
        padding-bottom: 5em;
        background-image: linear-gradient(90deg, #ffbb00, #ff137d),
        url('{{ asset("img/image.jpg")}}');
        background-attachment: fixed;
        background-size: cover;
        background-blend-mode: lighten;
        position: relative;
    }

    #wave {
        position: absolute;
        bottom: -1px;
    }

    svg {
        height: auto;
    }

    #content-banner h1,
    h2 {
        margin: 0;
        padding: 0;
    }

    #content-banner {
        position: relative;
        color: #fff;
        margin: auto;
        width: 70%;
        font-size: 1.4em;
        padding: 2em 2em 0 1em;

    }

    #btn-banner {
        padding-top: 1.3em;
        white-space: nowrap;
    }

    #btn-banner a {
        color: #fff;
        background-color: rgb(255, 205, 30);
        width: auto;
        padding: 0.5em 0.7em 0.5em 0.7em;
        border-top-left-radius: 30px;
        border-top-right-radius: 30px;
        border-bottom-left-radius: 30px;
        border-bottom-right-radius: 30px;
        font-size: 0.9em;
    }

    #btn-banner a:hover {
        background-color: rgb(255, 222, 102);
        color: #fff;
    }

    /**/
    .container-avatar-profile {
        width: 40px;
        height: 40px;
        border-radius: 900px;
        overflow: hidden;
    }

    .container-avatar-profile img {
        width: 100%;
        height: 100%;
    }

    /*MAIN*/
    main {
        padding: 2em 4em 2em 4em;
        width: 80%;
        margin: auto;
        height: auto;
        display: flex;
        flex-direction: column;

    }

    /*Artículo1*/
    #about-us {
        width: 100%;
    }



    #p1,
    #p2 {
        width: 100%;
    }

    #p2 img {
        width: 28em;
        height: auto;
        float: right;
    }

    #p1 h1 {
        color: rgb(250, 116, 66);
    }

    /*Artículo 2*/
    #article2 {
        width: 100%;
        font-size: 1.2em;
    }

    #article2 h1 {
        text-align: center;
        color: rgb(250, 116, 66);
    }

    #about-us h1 {
        text-align: center;
        color: rgb(250, 116, 66);
    }

    #article2 span {
        color: rgb(248, 162, 52);
    }

    #cols {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        margin-top: 3em;
    }

    #col1,
    #col2,
    #col3 {
        padding-right: 3em;
        text-align: justify;
        width: 100%;
        position: relative;
        /* box-shadow: 5px 5px 15px 5px rgba(0, 0, 0,0.05); */
        padding: 1.5em;
        margin-right: 2.5em;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        border-radius: 10px;
        transition: all 0.2s;
        background: white;
        box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.158);
    }

    #col1:hover,
    #col2:hover,
    #col3:hover {
        transform: scale(1.1);
        z-index: 2;
    }

    #cols p {
        padding-bottom: 10px;
        border-bottom: 3px solid rgb(255, 137, 58);
    }

    #cols i {
        font-size: 2.5em;
        margin-bottom: 0.4em;
        background: -webkit-linear-gradient(left,
                rgba(255, 93, 177, 1) 0%,
                rgb(255, 149, 29) 100%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
    }


    /*ARTICLE 3*/
    #sponsors {
        width: 100%;
        margin-top: 5em;
        font-size: 1.2em;
        text-align: justify;
    }

    #sponsors h1 {
        text-align: center;
        margin-bottom: 1em;
        color: rgb(250, 79, 108);
    }

    #sponsors-images img {
        width: 50px;
        height: 50px;
    }

    /*FOOTER*/

    footer {
        margin-top: 5em;
        height: 10em;
        position: relative;
        width: 100%;
        background: white;

    }

    #liquid-animation {
        position: relative;
        top: 0;
        z-index: 1;
        width: 100%;
        height: 2em;
        background: linear-gradient(to right, #ff00f2, #ffc400, #ff5e00, #c60bff);
        animation: animation 4s ease-in-out infinite;
        background-size: 200%;
    }

    @keyframes animation {
        0% {
            background-position: 0 50%;
        }

        50% {
            background-position: 100% 50%;
        }

        100% {
            background-position: 0 50%;
        }
    }

    #f-cols {
        font-size: 1.2em;
        padding: 2em 5em 2em 5em;
        display: flex;
        flex-direction: row;
        width: 100%;
        justify-content: space-between;
        background: white;

    }

    footer h2 {
        margin-bottom: 0.5em;
    }

    footer p {
        padding: 0em 5em 1em 5em;
        float: right;
    }

    .footer-col ul li a:hover {
        font-weight: bold;
    }

    #go-up {
        z-index: 2;
        background: #000;
        border-radius: 50%;
        bottom: 4em;
        display: block;
        height: 50px;
        position: fixed;
        right: 1em;
        width: 50px;
        color: #fff;
        cursor: pointer;
        padding: 10px;
    }

    #go-up:hover {
        background-color: #fff;
        color: #000;
        border: 3px solid #000;
        padding: 7px;
    }



    /*cookies*/
    .cookies-container {
        text-align: center;
        z-index: 9999;
        width: 100vw;
        height: 100vh;
        padding: 30px 30px 30px 30px;
        background: rgba(0, 0, 0, 0.7);
        overflow: hidden;
        position: fixed;
        bottom: 0px;
        /* display: none; */
    }

    .cookies-panel {
        border-radius: 10px;
        overflow-x: hidden;
        background-color: #fff;
        margin: 0 auto;
        width: 35vw;
        margin-top: 2em;
        height: auto;
        text-align: left;
        padding: 3em;
        max-height: 26.8em;
    }

    .cookieButton {
        margin-top: 2em;
    }

    .cookieButton a {
        border-radius: 7px;
        border: 1px solid #000;
        margin-top: 4px;
        padding: 1em;
    }

    .cookieButton a:hover {
        cursor: pointer;
        color: #fff;
        background-color: #000;
    }


    /*SCROLLBAR*/
    html::-webkit-scrollbar {
        width: 16px;
        height: 16px;
    }

    html::-webkit-scrollbar-thumb {
        background: #434343;
        border-radius: 16px;

        box-shadow: inset 2px 2px 2px hsl(0deg 0% 100% / 25%), inset -2px -2px 2px rgb(0 0 0 / 25%);
    }

    html::-webkit-scrollbar-track {

        background: linear-gradient(90deg, #434343, #434343 1px, #111 0, #111);
    }


    /*MÓVIL*/
    @media screen and (min-width: 0px) and (max-width: 540px) {
        /*HEADER*/

        /*alerta y menú*/
        #covid-alert-buttons {
            margin-top: 1em;
        }

        #covid-alert-buttons a {
            flex-direction: column;
            text-align: center;
        }


        #logo {
            display: flex;
            flex-direction: row;
        }

        #logo a {
            text-align: center;

        }

        #logo a img {
            width: 80%;
            height: 50%;
            margin-left: 0;
        }

        #menu {
            flex-direction: column;
            padding: 2em 1em 2em 1em;
        }

        #menu ul {
            /* flex-direction: column; */
            text-align: center;
        }

        #menu ul li {
            padding: 0.5em;
            margin-right: 0em;

        }

        #searcher {
            margin-top: 1em;
        }

        #menu ul {
            flex-direction: column;
        }

        #hamburger {
            display: block;
            font-size: 2em;
        }

        #menu ul li a:hover {
            font-weight: bold;
            border: none;
        }

        #col1:hover,
        #col2:hover,
        #col3:hover {
            transform: scale(1.05);
        }

        /*banner*/
        #content-banner {
            width: 100%;
            padding-left: 1em;
            padding-right: 1em;
            font-size: 1.3em;
            padding-bottom: 2em;
        }

        #banner {
            height: auto;
            padding-bottom: 2.5em;
        }


        /*MAIN*/
        main {
            padding: 1.5em;
            margin: auto;
            width: 100%;
            height: fit-content
        }

        #about-us {
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }

        #p1,
        #p2 {
            width: 100%;
            margin-bottom: 1em;
        }

        #p2 img {
            float: none;
            width: 100%;
        }

        /*article2*/
        #cols {
            flex-direction: column;
        }

        #col1,
        #col2,
        #col3 {
            padding-right: 0;
            margin-bottom: 3em;
            box-shadow: 5px 5px 15px 5px rgba(0, 0, 0, 0.1);
            padding: 1.5em;
            margin-right: 1em;
        }

        #sponsors {
            margin-top: 3em;
        }

        /*footer*/
        #f-cols {
            flex-direction: column;
            padding: 2em;
            background: white;

        }

        .footer-col {
            margin-bottom: 1em;

        }

        footer p {
            padding: 0 2em 2em 2em;
        }

        /*cookies*/
        .cookies-panel {
            width: 95%;
        }
    }

    /*TABLET*/
    @media screen and (min-width: 541px) and (max-width: 768px) {

        /*HEADER*/
        #hamburger {
            display: block;

        }

        #logo a img {
            width: 70%;
            margin-left: 0;
        }

        #logo a {
            text-align: center;
        }

        /*alerta y menú*/
        #covid-alert-buttons {
            margin-top: 1em;
        }

        #logo {
            margin-left: 0em;
        }

        #menu {
            flex-direction: column;
            padding: 2em 1em 2em 1em;
        }

        #menu ul {
            flex-direction: column;
            text-align: center;
        }

        #menu ul li {
            padding: 0.5em;
            margin-right: 0em;
        }

        #searcher {
            margin-top: 1em;
        }

        #menu ul li a:hover {
            font-weight: bold;
            border: none;
        }

        /*banner*/
        #content-banner {
            width: 100%;
            padding-left: 1em;
            padding-right: 1em;
            font-size: 1.3em;
        }

        #banner {
            height: auto;
            padding-bottom: 10em;
        }


        /*MAIN*/
        main {
            padding: 2em;
        }

        #about-us {
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }

        #p1,
        #p2 {
            width: 100%;
            margin-bottom: 1em;
        }

        #p2 img {
            float: none;
            width: 90%;
        }

        /*article2*/
        #cols {
            flex-direction: column;
        }

        #col1,
        #col2,
        #col3 {

            padding-right: 0;
            margin-bottom: 3em;
            box-shadow: 5px 5px 15px 5px rgba(0, 0, 0, 0.1);
            padding: 1.5em;
            margin-right: 1em;
        }

        #sponsors {
            margin-top: 3em;
        }

        /*footer*/
        #f-cols {
            flex-direction: column;
            padding: 2em;
            background: white;

        }

        .footer-col {
            margin-bottom: 1em;
        }

        footer p {
            padding: 0 2em 2em 2em;
            float: none;
            text-align: center;
        }

        .cookies-panel {
            width: 70%;
        }
    }

    /*AJUSTES ENTRE MOVIL Y TABLET*/
    @media screen and (min-width: 769px) {

        #logo {
            margin-left: 0em;
        }

        #menu {
            overflow: hidden;
            padding-left: 1em;
        }

        #menu input {
            width: 10em;
        }

        #menu ul {
            margin-left: 1em;
        }

        #menu ul li {
            padding-right: 0.4em;
        }

        #logo a img {
            width: 7em;
        }

        #logo {
            margin-right: 1em;
        }

        #about-us {
            flex-wrap: wrap;
        }

        #p2 img {
            width: 100%;
        }

        #banner {
            height: auto + 60em;
        }
    }


    /*DISPOSITIVOS GRANDES*/
    @media screen and (min-width: 1501px) {

        #banner {
            height: 40em;
        }

        main {
            margin: auto;
            width: 70%;
        }

        #menu ul li a {
            font-size: 1em;

        }

        #xmenu {
            margin-left: 100px;
        }

        #content-banner {
            font-size: 1.7em;
            width: 80%;
        }
    }
</style>

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Ocio de Zaragoza">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('title') - Ocio ZGZ</title>
    <!-- <link rel="stylesheet" href="{{ asset('css/app.css') }}" /> -->
    <link rel="stylesheet" stype="text/css" href="./css/normalize.css" />
    <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="./css/font-awesome/css/font-awesome.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type=" text/javascript" src="{{ asset('js/loader.js') }}"></script>
    <script type=" text/javascript" src="{{ asset('js/client.js') }}"></script>

    <script>
        /* ésto comprueba la localStorage si ya tiene la variable guardada */
        function compruebaAceptaCookies() {
            if (localStorage.aceptaCookies == 'true') {
                // cookies-container.style.display = 'none';
                $('.cookies-container').css('display', 'none');
            }
        }

        /* aquí guardamos la variable de que se ha
        aceptado el uso de cookies así no mostraremos
        el mensaje de nuevo */
        function aceptarCookies() {
            localStorage.aceptaCookies = 'true';
            $('.cookies-container').css('display', 'none');
        }

        /* ésto se ejecuta cuando la web está cargada */
        $(document).ready(function() {
            compruebaAceptaCookies();
            $('#close-alert').click(function() {
                $('.alert-message').css('display', 'none');
            });
        });
    </script>

</head>

<body onload="main()">

    <div class="pre-loader-container" id="loader">
        <div class="pre-loader">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
        <!-- <p style="font-size: 1.3em;">Cargando  ...</p> -->
    </div>
    <div class="container">
        <!--HEADER-->
        <header>
            <!--NAVEGACIÓN (Alerta covid & Menú)-->
            <!--ALERTA DE COVID-->
            <nav>
                <div id="covid-alert">
                    <p>COVID19: Todas las actividades se realizan siguiendo las medidas de seguridad.</p>
                    <div id="covid-alert-buttons">
                        @if (!Auth::check())
                        <div id="login" class="covid-alert-btn" style="margin-right:2em">
                            <a href="{{route('login')}}">
                                <i class="fa fa-sign-in" aria-hidden="true"></i>
                                <p>Iniciar sesión</p>
                            </a>
                        </div>
                        <div id="register" class="covid-alert-btn">
                            <a href="{{route('register')}}">
                                <i class="fa fa-user-plus" aria-hidden="true"></i>
                                <p>Registro</p>
                            </a>
                        </div>
                        @else
                        <div class="covid-alert-btn">
                            <a href="{{route('myProfile')}}" style="margin-right:2em;display: flex; flex-direction:row; align-items:center;">
                                <p style="margin-right:1em; color:white;font-size:0.9em">{{Auth::user()->nick}}</p>
                                <div class="container-avatar-profile">
                                    <img src="{{url('user/avatar/'.Auth::user()->image)}}" class="avatar">
                                </div>
                            </a>
                        </div>
                        <a href="/mis-reservas" style="color: white; margin-right:3em; font-size:0.9em"><i class="fa fa-ticket" style="margin-right: 10px;" aria-hidden="true"></i>Mis reservas</a>
                        @if (Auth::user()->role_id == 2)
                        <div class="covid-alert-btn" style="margin-right:2em">
                            <a href="{{route('welcome')}}">
                                <i class="fa fa-dashboard "></i>
                                <p>Panel de control</p>
                            </a>
                        </div>
                        @endif
                        <div class="covid-alert-btn">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out" aria-hidden="true"></i>
                                <p>Cerrar sesión</p>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                        @endif
                    </div>
                </div>

                <!--MENÚ-->
                <div id="menu">
                    <div id="logo">
                        <a href="/">
                            <img src="{{ asset('img/logo.svg') }}" alt="logo ocio zgz" />
                        </a>
                        <i class="fa fa-bars" id="hamburger" style="font-size: 45px; cursor: pointer;"></i>
                    </div>
                    <ul id="xmenu">
                        <li><a href="{{route('home')}}" class="activo"><strong>INICIO</strong></a></li>
                        <li><a href="{{route('colaboradores')}}"><strong>COLABORADORES</strong></a></li>
                        <li><a href="{{route('contactUs')}}"><strong>CONTACTO</strong></a></li>
                    </ul>
                    <!-- <div id="searcher">
                        <input type="text" name="search" id="search" placeholder="Buscar..." />
                    </div> -->
                </div>
            </nav>

            @yield('banner')

            <!--MAIN-->
            <main>
                @yield('content')
            </main>

            <!--FOOTER-->
            <footer>
                <div id="liquid-animation"></div>
                <div id="f-cols">
                    <div class="footer-col">
                        <h2>Ocio Zgz</h2>
                        <ul>
                            <li><a href="#">¿Qué es Ocio Zgz?</a></li>
                            <li><a href="#">Política de privacidad</a></li>
                            <!-- <li><a href="#">Términos y condiciones</a></li> -->
                            <li><a href="#">Cookies</a></li>
                            <li><a href="#">Compromiso</a></li>
                        </ul>
                    </div>

                    <div class="footer-col">
                        <h2>Sobre nosotros</h2>
                        <ul>
                            <li><a href="#">¿Quiénes somos?</a></li>
                            <li><a href="#">Contacto</a></li>
                        </ul>
                    </div>

                    <div class="footer-col">
                        <h2>Nuestras redes</h2>
                        <div style="display: flex; flex-direction:row;" id="social-nets">
                            <!-- <img src="{{ asset ('img/facebook.svg') }}" width="30px"> -->
                            <a style="margin-right:1em" href="https://www.linkedin.com/in/alberto-perales-lafuente-bb11231aa/" target="_blank"><img src="{{ asset ('img/linkedin.svg') }}" width="30px"></a>
                            <a href="https://github.com/albertoPerales" target="_blank"><img src="{{ asset ('img/github.svg') }}" width="30px"></a>
                            <!-- <img src="{{ asset ('img/twitter.svg') }}" width="30px"> -->
                        </div>
                    </div>
                </div>
                <p>&copy;Copyright <?= date("Y") ?> | Alberto Perales Lafuente</p>
            </footer>

            <div id="go-up">
                <i class="fa fa-arrow-up" style="font-size: 2em;"></i>
            </div>

            <div class="cookies-container">
                <div class="cookies-panel">
                    <div>
                        <h3>Cookies.</h3>
                        <p><br>¿Te gustan las cookies? &#x1F36A;</br></br>
                            Utilizamos cookies propias con objetivos funcionales de la aplicación. Si continúas navegando, asumimos que
                            aceptas su uso.
                            <!-- <a href="/privacy-policy/" target="_blank" style="text-decoration: underline">What for?</a> -->
                        </p>
                        <div class="cookieButton" id="btnCookie">
                            <a onclick="aceptarCookies()">Entendido</a>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </div>
    </div>

</body>

</html>