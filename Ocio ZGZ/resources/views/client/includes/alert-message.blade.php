@if(session('message'))
    <div id="display">
        <div class="alert-message" style="position: relative;">
            <i class="fa fa-check" style="border: 2px solid rgb(43, 159, 92); border-radius:50%;padding:0.5em; margin-right:1em"></i><strong>{{session('message')}}</strong>
            <i class="fa fa-times" id ="close-alert" style="position: absolute; right:20px; font-size:1.3em; cursor:pointer" ></i>
        </div>
    </div>
@endif