@extends('client.app')
@section('title', 'Mis reservas')
<!--Título dinámico-->
<style>
    table {
        width: 100%;
        border-collapse: collapse;
    }

    th,
    td {
        width: 25%;
        text-align: left;
        vertical-align: top;
        padding: 0.3em;
        caption-side: bottom;

        border-collapse: collapse;
    }

    td {
        text-align: right;
        border-bottom: 1px solid black;
        padding: 1em;
    }

    thead {
        background: linear-gradient(to left, #ffc62a, #ff62c6);
        color: white;
        font-weight: bold;
    }

    th {
        text-align: center;
    }
</style>

@section('content')

<h1>Mis reservas</h1>
<div style="margin-top:1em;height: 25em;
        overflow-y:auto">
    @if (isset($user->bookings))
    <table style="width: 70%;">
        <thead>
            <tr>
                <th>Evento</th>
                <th>Asistentes</th>
                <th>Precio</th>
                <th></th>
            </tr>
        </thead>
        @foreach ($user->bookings as $booking)
        <tr>

            <td>{{($booking->getEvent($booking->event_id))->name}}</td>
            <td>{{$booking->asistentes}}</td>
            <td>{{$booking->price}}€</td>
            <td><a href="/evento/{{$booking->event_id}}/" style="width: 100%; background: linear-gradient(to left,#ffc62a, #ff62c6); color: #fff; border-top-left-radius: 10px;border-top-right-radius: 10px;border-bottom-right-radius: 10px;border-bottom-left-radius: 10px; padding: 0.5em; text-align: center;margin-bottom: 0.5em; white-space:nowrap">Ver evento</a></td>
        </tr>
        @endforeach
    </table>
    @else
    <p style="font-size: 1.2em;">No has realizado ninguna reserva</p>
    @endif
</div>

</div>
@endsection